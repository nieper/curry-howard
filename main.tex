\documentclass[utf8]{beamer}

\usepackage[german]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{graphics}
\usepackage{proof}
\usepackage{turnstile}

\lstdefinelanguage{Scheme}
{
  mathescape=true,
  basicstyle=\ttseries,
  keywordstyle=\bfseries,
  escapechar=|,
  alsodigit=',
  alsoletter=*+?-<>\#,
  otherkeywords={'},
  morekeywords={define,car,cdr,cons,lambda,force,delay,let,if,quotient,zero?,call/cc-,<,+,*,not,\#t,\#f}
}
\lstset{language={Scheme}}

\mode<presentation>

\usetheme{CambridgeUS}
\usefonttheme{structureitalicserif}
\usecolortheme{crane}

%\AtBeginSection{\frame{\sectionpage}}

\title[Curry--Howard--Lambek]{Die Curry--Howard--Lambek-Korrespondenz}
\subtitle{Typen als Aussagen und Programme als Beweise}
\author[Marc Nieper-Wißkirchen]{Marc Nieper-Wißkirchen}
\institute[]{Institut für Mathematik\\Universität Augsburg}
\date[26.02.2017]{26.~Januar 2017}

\renewcommand{\implies}{\mathbin{\Rightarrow}}
\newcommand{\yields}{\sststile{}{}}
\newcommand{\lstmath}[1]{\text{\lstinline!#1!}}

\begin{document}
\lstset{gobble=6,basicstyle=\ttfamily\upshape}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Übersicht}
  \tableofcontents[hideallsubsections,pausesections]
\end{frame}

\section{Einführung}

\begin{frame}{Curry, Howard und Lambek}
  \begin{columns}[onlytextwidth]
    \only<1-3,4>{
      \begin{column}{0.3\textwidth}
        \begin{center}
          \includegraphics[width=\textwidth]{HaskellBCurry.jpg}
          \vfill
          \textsc{Haskell B.~Curry, 1900--1982}
        \end{center}      
      \end{column}
    }
    
    \only<2-3,5>{
      \begin{column}{0.3\textwidth}
        \begin{center}
          \includegraphics[width=\textwidth]{wahow.jpg}
          \vfill
          \textsc{William A.~Howard, 1926--}
        \end{center}
      \end{column}
    }

    \only<3-3,6>{
      \begin{column}{0.3\textwidth}<3-3,6>
        \begin{center}
          \includegraphics[width=\textwidth]{lambek1.jpg}
          \vfill
          \textsc{Joachim Lambek, 1922--2014}
        \end{center}
      \end{column}
    }

    \only<4-6>{
      \begin{column}{0.6\textwidth}
        \only<4>{
          \begin{center}
            \emph{Functionality in Combinatory Logic}, 1934:
            "`Kombinatorische Logik und der Hilbert-Kalkül sind
            äquivalent."'
          \end{center}
        }
        \only<5>{
          \begin{center}
            \emph{The formulae-as-types notion of
              construction}, 1969/80: "`Der einfach typisierte Lambda-Kalkül
            und der Kalkül des natürlichen Schließens sind
            äquivalent."'
          \end{center}
        }
        \only<6>{          
          \begin{center}
            \emph{Deductive systems and categories, III}, 1972:
            "`Der einfach getypte Lambda-Kalkül und die interne Logik einer kartesisch
            abgeschlossene Kategorie sind äquivalent."'
          \end{center}
        }
      \end{column}
    }
  \end{columns}
\end{frame}

\section{Positive intuistionistische Aussagenlogik}

\subsection{Junktoren}

\begin{frame}{Definitionen}
  \begin{block}{Konjunktion}
    Sind $A$ und $B$ zwei Aussagen, so ist $A \land B$ eine weitere
    Aussage, die \emph{Konjunktion} der Aussagen $A$ und $B$.
  \end{block}
  \pause
  \begin{block}{Implikation}
    Sind $A$ und $B$ zwei Aussagen, so ist $A \implies B$ eine weitere
    Aussage, die \emph{Implikation} von der Aussage $A$ zur Aussage $B$.
  \end{block}
  \pause
  \begin{block}{Tautologie}
    Die \emph{Tautologie} $\top$ ist eine Aussage.
  \end{block}
  \pause
  \begin{block}{Vereinbarung}
    Ab jetzt seien $A$, $B$, $C$, \dots Metavariablen, die für Aussagen stehen.
  \end{block}
\end{frame}

\begin{frame}{Regeln der Konjunktion}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{block}{Einführungsregel der Konjunktion}
        \begin{align*}
          \infer[(\land E)]{\Gamma \yields A \land B}{\Gamma \yields A & \Gamma \yields B}
        \end{align*}
      \end{block}
      \pause
      \begin{block}{Beseitigungsregeln der Konjunktion}
        \begin{align*}
          \infer[(\land B_L)]{\Gamma \yields A}{\Gamma \yields A \land B}
          & &
              \infer[(\land B_R)]{\Gamma \yields B}{\Gamma \yields A \land B}
        \end{align*}
      \end{block} 
    \end{column}
    \pause
    \begin{column}{0.35\textwidth}
      \begin{Beispiel}
        Wenn ich weiß, daß Skolem ein Norweger ist, und wenn ich weiß, daß Skolem ein Logiker ist,
        dann weiß ich auch, daß Skolem ein Norweger ist und Skolem ein Logiker ist.
      \end{Beispiel}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Regeln der Implikation}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{block}{Einführungsregel der Implikation}
        \begin{align*}
          \infer[(\implies E)]{\Gamma \yields A \implies B}{\Gamma, A \yields B}
        \end{align*}
      \end{block}
      \pause
      \begin{block}{Beseitigungsregel der Implikation}
        \begin{align*}
          \infer[(\implies B)]{\Gamma \yields B}{\Gamma \yields A & \Gamma \yields A \implies B}
        \end{align*}
      \end{block} 
    \end{column}
    \pause
    \begin{column}{0.35\textwidth}
      \begin{Beispiel}
        Wenn ich unter der Annahme, daß Skolem ein Skandinavier ist,
        weiß, daß Skolem ein Norweger ist, dann weiß ich, daß aus, daß
        Skolem ein Skandinavier ist, folgt, daß Skolem ein Norweger ist.
      \end{Beispiel}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Regel der Tautologie}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{block}{Einführungsregel der Tautologie}
        \begin{align*}
          \infer[(\top E)]{\yields \top}{}
        \end{align*}
      \end{block}
      \pause
      \begin{alertblock}{Bemerkung}
        Es gibt keine Beseitigungsregel der Tautologie.
      \end{alertblock}
    \end{column}
    \pause
    \begin{column}{0.35\textwidth}
      \begin{Beispiel}
        Ich weiß mindestens nichts.
      \end{Beispiel}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Beispiel}
  \begin{Satz}
    Zeige $((A \implies B) \land (B \implies C)) \implies (A \implies C)$:
    \pause
    \begin{align*}
      \onslide<2->{A, (A \implies B) \land (B \implies C) & \yields A \tag{Axiom} \\}
      \onslide<3->{
      A, (A \implies B) \land (B \implies C) & \yields (A \implies B) \land (B \implies C) 
                                               \tag{Axiom}\\}
      \onslide<4->{
      A, (A \implies B) \land (B \implies C) & \yields A \implies B \tag{$\land B_L$}\\}
      \onslide<5->{
      A, (A \implies B) \land (B \implies C) & \yields B \implies C \tag{$\land B_R$}\\}
      \onslide<6->{
      A, (A \implies B) \land (B \implies C) & \yields B \tag{$\implies B$}\\}
      \onslide<7->{
      A, (A \implies B) \land (B \implies C) & \yields C \tag{$\implies B$}\\}
      \onslide<8->{
      (A \implies B) \land (B \implies C) & \yields A \implies C \tag{$\implies E$}\\}
      \onslide<9->{& 
                     \yields ((A \implies B) \land (B \implies C)) 
                     \implies (A \implies C) \tag{$\implies E$}\\}
    \end{align*}
  \end{Satz}
\end{frame}

\subsection{Brouwer--Heyting--Kolmogorow-Interpretation}

\begin{frame}{Interpretation der Konjunktion}
  \begin{block}{Regeln}
    \begin{align*}
      \infer[(\land E)]
      {\Gamma \yields \only<3->{\alert<3>{\text{\lstinline{(cons a b)}}\in}} A \land B}
      {\Gamma \yields \only<3->{\alert<3>{\text{\lstinline{a}}\in}} A
      & \Gamma \yields \only<3->{\alert<3>{\text{\lstinline{b}}\in}} B}
    \end{align*}
    \begin{align*}
      \infer[(\land B_L)]{\Gamma \yields \only<4->{\alert<4>{\text{\lstinline{(car p)}}\in}} A}
      {\Gamma \yields \only<4->{\alert<4>{\text{\lstinline{p}}\in}} A \land B} 
      & & \infer[(\land B_R)]{\Gamma \yields \only<5->{\alert<5>{\text{\lstinline{(cdr p)}}\in}} B}
          {\Gamma \yields \only<4->{\alert<5>{\text{\lstinline{p}}\in}} A \land B}
    \end{align*}
  \end{block} 
  \pause
  \begin{block}{Interpretation}
    Ein Beweis von $A \land B$ ist ein Paar \lstinline{(cons a b)},
    bestehend aus einem Beweis \lstinline{a} von $A$ und einem Beweis \lstinline{b} von $B$.
  \end{block}
\end{frame}

\begin{frame}{Interpretation der Implikation}
  \begin{block}{Regeln}
    \begin{align*}
      \infer[(\implies E)]{\Gamma \yields
      \only<3->{\alert<3>{\lstmath{(lambda (a) b)}\in}} A \implies B}
      {\Gamma, \only<3->{\alert<3>{\lstmath{a}\in}} A \yields 
      \only<3->{\alert<3>{\lstmath{b}\in}} B}
    \end{align*}
    \begin{align*}
      \infer[(\implies B)]
      {\Gamma \yields \only<4->{\alert<4>{\lstmath{(f a)}\in}} B}
      {\Gamma \yields \only<4->{\alert<4>{\lstmath{a}\in}} A 
      & \Gamma \yields \only<4->{\alert<4>{\lstmath{f}\in}} A \implies B}
    \end{align*}
  \end{block}
  \pause
  \begin{block}{Interpretation}
    Ein Beweis von $A \implies B$ ist eine Prozedur
    \lstinline{(lambda (a) b)}, die einen Beweis \lstinline{a} von
    $A$ annimmt und einen Beweis \lstinline{b} von $B$ zurückliefert.
  \end{block}
\end{frame}

\begin{frame}{Interpretation der Tautologie}
  \begin{block}{Regel}
    \begin{align*}
      \infer[(\top E)]{\yields \only<3->{\alert<3>{\lstmath{'()}\in}} \top}{}
    \end{align*}
  \end{block}
  \pause
  \begin{block}{Interpretation}
    Es gibt einen Beweis \lstinline{'()} der Tautologie $\top$.
  \end{block}
\end{frame}

\begin{frame}{Beispiel}
  \begin{block}{Konstruktion}
    Konstruiere $((A \implies B) \land (B \implies C)) \implies (A \implies C)$:
    \pause
    \begin{small}
    \begin{gather*}
      \onslide<2->{\lstmath{a} \in A, \lstmath{p} \in (A \implies B) \land (B \implies C) \yields \lstmath{a} \in A \\}
      \onslide<3->{
      \lstmath{a} \in A, \lstmath{p} \in (A \implies B) \land (B \implies C) \yields \lstmath{p} \in (A \implies B) \land (B \implies C) \\}
      \onslide<4->{
      \lstmath{a} \in A, \lstmath{p} \in (A \implies B) \land (B \implies C) \yields \lstmath{(car p)} \in A \implies B \\}
      \onslide<5->{
      \lstmath{a} \in A, \lstmath{p} \in (A \implies B) \land (B \implies C) \yields \lstmath{(cdr p)} \in B \implies C \\}
      \onslide<6->{
      \lstmath{a} \in A, \lstmath{p} \in (A \implies B) \land (B \implies C) \yields \lstmath{((car p) a)} \in B \\}
      \onslide<7->{
      \lstmath{a} \in A, \lstmath{p} \in (A \implies B) \land (B \implies C) \yields \lstmath{((cdr p) ((car p) a))} \in C \\}
      \onslide<8->{
      \lstmath{p} \in
      (A \implies B) \land (B \implies C) \yields 
                                            \lstmath{(lambda (a) ((cdr p) ((car p) a)))} \in
                                            A \implies C \\}
      \onslide<9->{\begin{split}
          \yields \lstmath{(lambda (p) (lambda (a) ((cdr p) ((car p) a))))} \in\\
          ((A \implies B) \land (B \implies C)) 
          \implies (A \implies C)
        \end{split}}
    \end{gather*}
    \end{small}
  \end{block}
\end{frame}

\subsection{Kategorielle Äquivalenzen}

\begin{frame}{Curry--Howard-Korrespondenz}
  \begin{block}{Curry--Howard}
    Identifizieren wir eine Aussage in der positiven
    intuistionistischen Aussagenlogik mit dem Typ aller ihrer Beweise,
    so entsprechen die Beweise der Aussage genau den (wohltypisierten)
    Programmen (Termen) dieses Typs im Lambda-Kalkül.
  \end{block}
\end{frame}

\begin{frame}{Curry--Howard--Lambek-Korrespondenz}
  \begin{block}{Lambek}
    \begin{itemize}[<+->]
    \item
      Typen entsprechen Objekten in einer Kategorie.
    \item
      Terme vom Typ $A$ entsprechen Morphismen nach $A$.
    \item
      Der Typ $A \land B$ entspricht einem kategoriellen Produkt $A \times B$.
    \item
      Der Typ $A \implies B$ entspricht einem Exponentialobjekt $B^A$.
    \item
      Der Typ $\top$ entspricht einem terminalen Objekt $1$.
    \end{itemize}
    \pause[\thebeamerpauses]
    Positive intuistionistische Aussagenlogik und der einfach
    typisierte Lambda-Kalkül werden durch eine kartesisch
    abgeschlossene Kategorie beschrieben.
  \end{block}
\end{frame}

\section{Intuistionistische Aussagenlogik}

\subsection{Junktoren}

\begin{frame}{Definitionen}
  \begin{block}{Disjunktion}
    Sind $A$ und $B$ zwei Aussagen, so ist $A \lor B$ eine weitere
    Aussage, die \emph{Disjunktion} der Aussagen $A$ und $B$.
  \end{block}
  \pause
  \begin{block}{Kontradiktion}
    Die \emph{Kontradiktion} $\bot$ ist eine Aussage.
  \end{block}
  \pause
  \begin{Definition}
    Die Aussage $A \implies \bot$ kürzen wir mit $\lnot A$ ab.
  \end{Definition}
\end{frame}

\begin{frame}{Regeln der Disjunktion}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{block}{Einführungsregeln der Disjunktion}
        \begin{align*}
          \infer[(\lor E_L)]{\Gamma \yields A \lor B}{\Gamma \yields A}
          && \infer[(\lor E_R)]{\Gamma \yields A \lor B}{\Gamma \yields B}
        \end{align*}
      \end{block}
      \pause
      \begin{block}{Beseitigungsregel der Disjunktion}
        \begin{align*}
          \infer[(\lor B)]{\Gamma \yields C}{\Gamma \yields A \lor B & \Gamma, A \yields C
          & \Gamma, B \yields C}
        \end{align*}
      \end{block} 
    \end{column}
    \pause
    \begin{column}{0.35\textwidth}
      \begin{Beispiel}
        Wenn ich weiß, daß Skolem ein Norweger ist oder Skolem ein
        Schwede ist, und wenn ich unter der Annahme, daß Skolem ein
        Norweger (Schwede) ist, weiß, daß Skolem ein Skandinavier ist,
        dann weiß ich auch, daß Skolem ein Skandinavier ist.
      \end{Beispiel}
    \end{column}
  \end{columns} 
\end{frame}

\begin{frame}{Regel der Kontradiktion}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{block}{Beseitigungsregel der Kontradiktion}
        \begin{align*}
          \infer[(\bot B)]{\Gamma \yields A}{\Gamma \yields \bot}
        \end{align*}
      \end{block}
      \pause
      \begin{alertblock}{Bemerkung}
        Es gibt keine Einführungsregel der Kontradiktion.
      \end{alertblock}
    \end{column}
    \pause
    \begin{column}{0.35\textwidth}
      \begin{Beispiel}
        Wenn ich weiß, daß jede Aussage stimmt, dann weiß ich auch, daß Skolem
        ein Italiener war.
      \end{Beispiel}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Brouwer--Heyting--Kolmogorow-Interpretation}

\begin{frame}{Interpretation der Disjunktion}
  \begin{block}{Regeln}
    \begin{small}
    \begin{align*}
      \infer[(\lor E_L)]{\Gamma \yields
      \only<3->{\alert<3>{\lstmath{(cons \#t a)}\in}} A \lor B}
      {\Gamma \yields \only<3->{\alert<3>{\lstmath{a}\in}} A}
      && \infer[(\lor E_R)]{\Gamma \yields 
         \only<4->{\alert<4>{\lstmath{(cons \#f b)}\in}}A \lor B}{\Gamma 
         \yields \only<4->{\alert<4>{\lstmath{b}\in}} B}
    \end{align*}
    \begin{align*}
      \infer[\only<-4>{(\lor B)}]
      {\Gamma \yields \only<5->{\alert<5>{\lstmath{(if (car p) (let ((a (cdr p))) s) (let ((b (cdr p))) t))}\in}} C}
      {\Gamma \yields \only<5->{\alert<5>{\lstmath{p}\in}} A \lor B 
      & \Gamma, \only<5->{\alert<5>{\lstmath{a}\in}} A \yields \only<5->{\alert<5>{\lstmath{s}\in}}C
      & \Gamma, \only<5->{\alert<5>{\lstmath{b}\in}} B \yields \only<5->{\alert<5>{\lstmath{t}\in}}C}
    \end{align*}
    \end{small}
  \end{block}
  \pause
  \begin{block}{Interpretation}
    Ein Beweis von $A \lor B$ ist entweder ein Paar der Form \lstinline{(cons
      \#t a)} mit einem Beweis \lstinline{a} von $A$ oder der Form
    \lstinline{(cons \#f b)} mit einem Beweis \lstinline{b} von $B$.
  \end{block}
\end{frame}

\begin{frame}{Interpretation der Kontradiktion}
  \begin{block}{Regeln}
    \begin{align*}
      \infer[(\bot B)]{\Gamma \yields \only<3->{\alert<3>{\lstmath{(error "$\bot$")}\in}} A}
      {\Gamma \yields \only<3->{\alert<3>{\lstmath{(error "$\bot$")}\in}} \bot}
    \end{align*}
  \end{block}
  \pause
  \begin{block}{Interpretation}
    Ein Beweis von $\bot$ ist ein Beweis einer jeden Aussage. 
  \end{block}
\end{frame}

\begin{frame}{Curry--Howard--Lambek--Korrespondenz}
  \begin{block}{Lambek}
    \begin{enumerate}[<+->]
    \item
      Der Typ $A \lor B$ entspricht einer kategoriellen Summe $A + B$.
    \item
      Der Typ $\bot$ entspricht einem initialen Objekt $0$.
    \end{enumerate}
    \pause[\thebeamerpauses]
    Intuistionistische Aussagenlogik (und der einfach typisierte Lambda-Kalkül mit Summentypen)
    werden durch eine bikartesisch abgeschlossene Kategorie beschrieben.
  \end{block}
\end{frame}

\subsection{Klassische Logik}

\begin{frame}{Peircesches Gesetz}
  \begin{itemize}[<+->]
  \item
    Aus den bisherigen Regeln können wir die Gültigkeit von
    $A \lor \lnot A$ nicht ableiten.
  \item
    Die Logik, die wir erhalten, ist damit intuistionistisch (konstruktiv).
  \item
    Die Annahme weiterer Axiome kann unsere Logik aber klassisch machen.
  \end{itemize}
  \pause[\thebeamerpauses]
  \begin{Definition}
    Das \alert{Peircesche Gesetz} ist die Aussage $((A \implies B) \implies A) \implies A$. 
  \end{Definition}
  \pause
  \begin{block}{Einführungsregel}
    \begin{align*}
      \infer[(\text{Peirce})]{\yields \only<6->{\alert<6->{\lstmath{call/cc}\in}}((A \implies B) \implies A) \implies A}{}
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}{Ein Lemma}
  \begin{Lemma}
    Es gelte $A \lor C \implies B$.  Zeige: $A \implies B$.
    \pause
    \begin{align*}
      \onslide<2->{
      A \lor C \implies B, A & \yields A \lor C \implies B \tag{Axiom} \\}
      \onslide<3->{
      A \lor C \implies B, A & \yields A \tag{Axiom} \\}
      \onslide<4->{
      A \lor C \implies B, A & \yields A \lor C \tag{$\lor E_L$} \\}
      \onslide<5->{
      A \lor C \implies B, A & \yields B \tag{$\implies B$} \\}
      \onslide<6->{
      \only<7->{\alert<7>{\lstmath{f}\in}} A \lor C \implies B 
                             & \yields \only<7->{\alert<7>{\lstmath{(lambda (a) (f (cons \#t a)))}\in}}
                                          A \implies B \tag{$\implies E$}\\}
    \end{align*}
  \end{Lemma}
\end{frame}

\begin{frame}{Herleitung des Gesetzes vom ausgeschlossenen Dritten}
  \begin{Satz}
    Sei $A$ die Aussage $P \lor \lnot P$.  Zeige $A$:
    \pause
    \begin{small}
      \begin{gather*}
        \onslide<2->{
        P \implies \bot \yields \lnot P \tag{Axiom} \\}
      \onslide<3->{\yields (P \implies \bot) \implies \lnot P \tag{$\implies E$} \\}
        \onslide<4->{
        (P \lor \lnot P) \implies \bot \yields P \implies \bot \tag{Lemma} \\}
        \onslide<5->{
        (P \lor \lnot P) \implies \bot \yields \lnot P \tag{$\implies B$} \\}
        \onslide<6->{
        (P \lor \lnot P)\implies \bot \yields P \lor \lnot P \tag{$\lor E_L$} \\}
        \onslide<7->{
                        \yields (A \implies \bot) \implies A \tag{$\implies E$} \\}
        \onslide<8->{ \yields ((A \implies \bot) \implies A) \implies A
                        \tag{Peirce} \\}
        \onslide<9->{\yields\only<10->{\alert<10>{
                       \lstmath{(call/cc (lambda (c) (cons \#f (lambda (a) (c (cons \#t a))))))}
                        \in}} \\ P \lor \lnot P \tag{$\implies B$} \\}
    \end{gather*}
  \end{small}
\end{Satz}
\end{frame}

\begin{frame}{Was ist \lstinline{call/cc}?}
  \begin{align*}
    \lstmath{(call/cc (lambda (c) e))}
  \end{align*}
  \pause
  \begin{itemize}[<+->]
  \item Ist $c$ nicht frei in $e$ (oder ist $c$ nicht aufgerufen
    worden), so ist der Ausdruck äquivalent zu \lstinline{e}.
  \item Wird \lstinline{(c a)} ausgewertet, so springt das Programm
    an den Zeitpunkt der Auswertung des obigen Ausdruckes zurück,
    welcher den Wert $x$ bekommt.
  \item Programmiersprachen, die \lstinline{call/cc} implementieren
    sind: Scheme, Standard ML, Ruby, Unlambda.
  \end{itemize}
\end{frame}

\begin{frame}{Anwendungen von \lstinline{call/cc}}
  \begin{itemize}[<+->]
  \item Mit \lstinline{call/cc} lassen sich Ausnahmen, Generatoren,
    Koroutinen und präemptives Multitasking, \dots implementieren.
  \item Mit Hilfe von \lstinline{call/cc} läßt sich der
    nicht-deterministische \lstinline{amb}-Operator definieren:  Der Ausdruck
    \begin{align*}
      \lstmath{(amb $\ \langle\text{expr}\rangle \dots$)}
    \end{align*}
    wählt einen der Ausdrücke $\langle\text{expr}\rangle$ aus, wertet ihn aus
    und liefert sein Ergebnis zurück, und zwar so, daß das gesamte
    Programm konvergiert.  Der Ausdruck ist ein Fehler, wenn dies
    nicht möglich ist.
  \end{itemize}
  \pause[\thebeamerpauses]
  \begin{Beispiel}
    \begin{align*}
      \lstmath{(if (amb f t) 1 (amb))} \quad\Longrightarrow \quad 1
    \end{align*}
  \end{Beispiel}
\end{frame}

\section{Intuistionistische Prädikatenlogik}

\subsection{Quantoren}

\begin{frame}{Definitionen}
  \begin{block}{Allquantor}
    Sind $A$ und $B$ Aussagen und ist $x$ eine Variable vom Typ $A$, so ist
    die \emph{Allquantifizierung} $\forall x \in A\colon B$ eine weitere Aussage.
  \end{block}
  \pause
  \begin{block}{Existenzquantor}
    Sind $A$ und $B$ Aussagen und ist $x$ eine Variable vom Typ $B$, so ist
    die \emph{Existenzquantifizierung} $\exists x \in A\colon B$ eine weitere Aussage.
  \end{block}
  \pause
  \begin{block}{Vereinbarung}
    Ab jetzt seien $x$, $y$, $z$, \dots Metavariablen für Variable.  Taucht eine
    neue Metavariable auf, so bezeichne sie eine frische, bisher noch
    nicht vorgekommene Variable.
    \\
    \pause
    Weiter seien $a$, $b$, $c$, \dots Metavariablen für Terme.
    Ist $x$ eine Variable vom Typ $A$,
    so bezeichne $B(t/x)$ das Ersetzen aller freien Vorkommen von $x$ in $B$
    durch einen Term $t$ vom Typ $A$.
  \end{block}
\end{frame}

\begin{frame}{Regeln für den Allquantor}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{block}{Einführungsregel des Allquantors}
        \begin{align*}
          \infer[(\forall E)]{\Gamma \yields \forall x \in A\colon B}{\Gamma \yields B(y/x)}
        \end{align*}
      \end{block}
      \pause
      \begin{block}{Beseitigungsregel des Allquantors}
        \begin{align*}
          \infer[(\forall B)]{\Gamma \yields B(t/x)}{\Gamma \yields \forall x \in A\colon B}
        \end{align*}
      \end{block}
    \end{column}
    \pause
    \begin{column}{0.35\textwidth}
      \begin{Beispiel}
        Wenn ich weiß, daß alles Irdische vergänglich ist, dann weiß ich auch,
        daß Skolem vergänglich ist.
      \end{Beispiel}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Regeln für den Existenzquantor}
  \begin{columns}[onlytextwidth]
    \begin{column}{0.55\textwidth}
      \begin{block}{Einführungsregel des Existenzquantors}
        \begin{align*}
          \infer[(\exists E)]{\yields \exists x \in A\colon B}{\yields B(t/x)}
        \end{align*}
      \end{block}
      \pause
      \begin{block}{Beseitigungsregel des Existenzquantors}
        \begin{align*}
          \infer[(\exists B)]{\Gamma \yields C}
          {\Gamma \yields \exists x \in A\colon B & \Gamma, B(y/x) \yields C}
        \end{align*}
      \end{block}
    \end{column}
    \pause
    \begin{column}{0.35\textwidth}
      \begin{Beispiel}
        Wenn ich weiß, daß Skolem ein Norweger war, dann weiß ich, daß es Norweger gab.
      \end{Beispiel}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Brouwer--Heyting--Kolmogorow-Interpretation}

\begin{frame}{Interpretation des Allquantors}
  \begin{block}{Regeln}
    \begin{align*}
      \infer[(\forall E)]{\Gamma \yields \only<3->{\alert<3>{\lstmath{(lambda (y) b)} \in }}
      \forall x \in A\colon B}
      {\Gamma \yields \only<3->{\alert<3>{\lstmath{b} \in}} B(y/x)}
    \end{align*}
    \begin{align*}
      \infer[(\forall B)]{\Gamma \yields \only<4->{\alert<4>{\lstmath{(f t)} \in}} B(t/x)}
      {\Gamma \yields \only<4->{\alert<4>{\lstmath{f} \in}} \forall x \in A\colon B}
    \end{align*}   
  \end{block}
  \pause
  \begin{block}{Interpretation}
    Ein Beweis von $\forall x \in A\colon B$ in eine Prozedur
    \lstinline{(lambda (x) b)}, die einen Beweis $x$ von $A$ annimmt
    und einen Beweis von $B$ zurückliefert.
  \end{block}
  \visible<5>{
    \begin{itemize}
    \item
      $A \implies B$ ist ein Spezialfall von $\forall x \in A\colon B$.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Interpretation des Existenzquantors}
  \begin{block}{Regeln}
    \begin{align*}
      \infer[(\exists E)]{\Gamma \yields \only<3->{\alert<3>{\lstmath{(cons t b)} \in}} \exists x \in A\colon B}{\Gamma \yields \only<3->{\alert<3>{\lstmath{b} \in}} B(t/x)}
    \end{align*}
    \begin{align*}
      \infer[(\exists B)]{\Gamma \yields \only<4->{\alert<4>{\lstmath{(let ((y (car p)) (b (cdr p))) c)} \in}} C}
      {\Gamma \yields \only<4->{\alert<4>{\lstmath{p} \in}} \exists x \in A\colon B 
      & \Gamma, \only<4->{\alert<4>{\lstmath{b} \in}} B(y/x) \yields \only<4->{\alert<4>{\lstmath{c} \in}} C}
    \end{align*}
  \end{block}
  \pause
  \begin{block}{Interpretation}
    Ein Beweis von $\exists x \in A\colon B$ ist ein Paar \lstinline{(cons x b)}, wobei $x$ ein
    Beweis von $A$ ist und $b$ ein Beweis von $B$.
  \end{block}
  \visible<5>{
    \begin{itemize}
    \item
      $A \land B$ ist ein Spezialfall von $\exists x \in A\colon B$.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Curry--Howard--Korrespondenz}
  \begin{block}{Curry--Howard}
    Identifizieren wir eine Aussage in der intuistionistischen
    Prädikatenlogik mit dem Typ aller ihrer Beweise, so entsprechen
    die Beweise der Aussage genau den (wohltypisierten) Programmen
    (Termen) dieses Typs im Lambda-Kalkül mit abhängigen Typen.
  \end{block}
\end{frame}

\begin{frame}{Curry--Howard--Lambek-Korrespondenz}
  \begin{block}{Lambek}
    \begin{enumerate}[<+->]
    \item
      Typen, in denen eine Variable vom Typ $A$ frei vorkommt,
      entsprechen Objekten in der Scheibenkategorie über $A$.
    \item Terme vom Typ $B$, in denen eine Variable vom Typ $A$ frei
      vorkommt, entsprechen Morphismen über $A$.
    \item Der Typ $\forall x \in A\colon B$ entspricht $f_* B$, wobei
      $f_*$ der rechtsadjungierte Funktor zu $f^*\colon C \mapsto (A \times C \to A)$ ist.
    \item Der Typ $\exists x \in A\colon B$ entspricht $f_! B$, wobei
      $f_!$ der linksadjungierte Funktor zu $f^*$ ist.
    \end{enumerate}
    \pause[\thebeamerpauses]
    Intuistionistische Prädikatenlogik und der Lambda-Kalkül mit
    abhängigen Typen werden durch eine lokal kartesisch abgeschlosse
    bikartesische Kategorie beschrieben.
  \end{block}
\end{frame}

\begin{frame}[fragile]{Ein Beispiel}
  Unter den zusätzlichen Schlußregeln
  \begin{align*}
    \infer{\Gamma \yields \lstmath{(not $\ \omega$)} \in \Omega}{\Gamma \yields \lstmath{$\omega$} \in \Omega}
    && \infer{\Gamma \yields \lstmath{'()} \in \lstmath{$\omega$} \neq \lstmath{(not $\ \omega$)}}
       {\Gamma \yields \lstmath{$\omega$} \in \Omega}
  \end{align*}
  \pause
  haben wir den Satz
  \begin{align*}
    \forall \lstmath{f} \in X \implies (X \implies \Omega)\colon
    \exists \lstmath{g} \in X \implies \Omega\colon
    \forall \lstmath{x} \in X \colon
    \exists \lstmath{y} \in X: \lstmath{((f x) y)} \neq \lstmath{(g y)}
  \end{align*}
  \pause
  mit Beweis:
  \begin{lstlisting}
      (lambda (f)
        (let ((g (lambda (y)
                   (not ((f y) y)))))
          (cons g (lambda (x)
                    (let ((y x))
                      (cons y '()))))))
  \end{lstlisting}
\end{frame}

\section{Zusammenfassung}

\begin{frame}{Zusammenfassung}
  \begin{itemize}[<+->]
  \item Um intuitionistisch einen Beweis von $A \lor B$ angeben zu
    können, muß ich mindestens einen Beweis von $A$ oder einen Beweis
    von $B$ angeben können.
  \item Haben wir eine intuistionistischen Beweis für eine Aussage
    gefunden, so haben wir nichts anderes gemacht, als ein korrektes
    Computerprogramm zu schreiben, welches das durch die Aussage beschriebene Problem löst.
  \item Wollen wir eine Programmiersprache erfinden, müssen wir
    Kalküle und Typsysteme studieren. Aber dafür sollten wir Kategorien von
    Kategorien, also Kategorientheorie, studieren.  (Etwa symmetrisch
    monoidale Kategorien für lineare Logik.)
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
