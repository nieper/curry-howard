(define-library (amb)
  (export amb)
  (import (scheme base))
  (begin
    (define (amb-fail)
      (error "amb: fail"))

    (define-syntax amb 
      (syntax-rules () 
	((amb x ...) 
	 (let ((old-fail amb-fail)) 
	   (call/cc 
	    (lambda (return) 
	      (call/cc 
	       (lambda (fail) 
		 (set! amb-fail fail)
		 (return x)))
	      ...
	      (set! amb-fail old-fail)
	      (amb-fail)))))))))
