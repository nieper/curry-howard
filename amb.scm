(import (scheme base)
	(scheme write)
	(amb))

(define-syntax assert!
  (syntax-rules ()
    ((assert! condition)
     (unless condition
       (amb)))))

(define (choose-color) (amb 'Rot 'Gelb 'Blau 'Grün))

(define europe
  '((Portugal Spanien)
    (Spanien Frankreich Portugal)
    (Frankreich Spanien Italien Schweiz Belgien Deutschland Luxemburg)
    (Belgien Frankreich Niederlande Luxemburg Deutschland)
    (Niederlande Belgien Deutschland)
    (Deutschland Frankreich Österreich Schweiz Niederlande Belgien Luxemburg)
    (Luxemburg Frankreich Belgien Deutschland)
    (Italien Frankreich Österreich Schweiz)
    (Schweiz Frankreich Italien Österreich Deutschland)
    (Österreich Italien Schweiz Deutschland)))

(define (color-europe)
  (let ((colors (map (lambda (neighbors)
		       (cons (car neighbors) (choose-color)))
		     europe)))
    (for-each (lambda (color neighbors)
		(let ((country (car neighbors)))
		  (for-each (lambda (neighbor)
			      (assert! (not (eq? (cdr color)
						 (cdr (assq neighbor colors))))))
			    (cdr neighbors))))
	      colors europe)
    colors))

(display (color-europe))
(newline)

